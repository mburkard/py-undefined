"""Py-Undefined unit tests."""
import inspect
import typing
from typing import Union

from py_undefined import Undefined, UndefinedType


def test_comparison() -> None:
    assert isinstance(Undefined, UndefinedType)
    assert Undefined is Undefined
    assert Undefined == Undefined
    assert (Undefined or None) is None


def test_annotations() -> None:
    a: Union[Undefined, int] = Undefined
    b: Union[Undefined, int] = 1
    c: Union[Undefined, int] = 1.5

    # noinspection PyUnusedLocal
    def _undefined_param(param: Union[str, Undefined]) -> None:
        """Confirms Undefined is a type."""

    signature = inspect.signature(_undefined_param)
    assert Undefined in typing.get_args(signature.parameters["param"].annotation)

    # noinspection PyUnusedLocal
    def _undefined_param_310(param: str | Undefined) -> None:
        """Confirms Undefined is a type."""

    signature = inspect.signature(_undefined_param_310)
    assert Undefined in typing.get_args(signature.parameters["param"].annotation)


def test_representations() -> None:
    assert str(Undefined) == "Undefined"
    assert repr(Undefined) == "Undefined"
